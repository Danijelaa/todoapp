package com.toDoApp.model;

public class Task {

	private static Long index=9L;
	private Long id;
	private String title;
	private TaskState taskState;
	
	public Task() {
		super();
	}
	public Task(String title, TaskState taskState) {
		super();
		this.title = title;
		this.taskState = taskState;
	}
	public Task(Long id, String title, TaskState taskState) {
		super();
		this.id = id;
		this.title = title;
		this.taskState = taskState;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public TaskState getTaskState() {
		return taskState;
	}
	public void setTaskState(TaskState taskState) {
		this.taskState = taskState;
	}
	public static Long getIndex() {
		return index;
	}
	public static void setIndex(Long index) {
		Task.index = index;
	}
	
	
}
