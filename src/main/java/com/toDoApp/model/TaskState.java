package com.toDoApp.model;

public class TaskState {

	private static Long index=5L;
	private Long id;
	private String title;
	private Dashboard dashboard;
	
	public TaskState() {
		super();
	}
	public TaskState(String title, Dashboard dashboard) {
		super();
		this.title = title;
		this.dashboard = dashboard;
	}
	public TaskState(Long id, String title, Dashboard dashboard) {
		super();
		this.id = id;
		this.title = title;
		this.dashboard = dashboard;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Dashboard getDashboard() {
		return dashboard;
	}
	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}
	public static Long getIndex() {
		return index;
	}
	public static void setIndex(Long index) {
		TaskState.index = index;
	}
	
	
}
