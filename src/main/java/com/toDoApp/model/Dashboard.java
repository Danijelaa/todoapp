package com.toDoApp.model;

public class Dashboard {

	private static Long index=3L;
	private Long id;
	private String title;
	private User user;
	//
	public Dashboard() {
		super();
	}
	public Dashboard(String title, User user) {
		super();
		this.title = title;
		this.user = user;
	}
	//da tagujemgguzuujpppp
	public Dashboard(Long id, String title, User user) {
		super();
		this.id = id;
		this.title = title;
		this.user = user;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public static Long getIndex() {
		return index;
	}
	public static void setIndex(Long index) {
		Dashboard.index = index;
	}
	
	
}
