package com.toDoApp;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toDoApp.model.Dashboard;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.model.User;
import com.toDoApp.service.DashboardService;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.service.UserService;
import com.toDoApp.support.WriteIntoFiles;
@Component
public class TestData {

	public static List<User> users=new ArrayList<>();
	public static List<Dashboard> dashboards=new ArrayList<>();
	public static List<TaskState> tStates=new ArrayList<>();
	public static List<Task> tasks=new ArrayList();
	@Autowired
	private WriteIntoFiles write;
	@Autowired
	private UserService userService;
	@Autowired
	private DashboardService dashboardService;
	@Autowired
	private TaskStateService taskStateService;
	
	@PostConstruct
	public void initial(){
		System.out.println("Jeiiii");
		User user1=new User(1L,"user1", "password1");
		Dashboard dashboard1=new Dashboard(1L, "dashboard1OfUser1", user1);
		
		TaskState taskstate1=new TaskState(1L,"taskState1OfDashboardOfUser1", dashboard1);
		Task task1=new Task(1L, "task1OfTaskState1OfDashboardOfUser1", taskstate1);
		Task task2=new Task(2L, "task2OfTaskState1OfDashboardOfUser1", taskstate1);
		
		TaskState taskstate2=new TaskState(2L, "taskState2OfDashboardOfUser1", dashboard1);
		Task task3=new Task(3L, "task3OfTaskState2OfDashboardOfUser1", taskstate2);
		Task task4=new Task(4L, "task4OfTaskState2OfDashboardOfUser1", taskstate2);
		
		
		//data for user2
		User user2=new User(2L, "user2", "password2");
		Dashboard dashboard2=new Dashboard(2L,"dashboard2OfUser2", user2);
		TaskState taskstate3=new TaskState(3L, "taskState1OfDashboardOfUser2", dashboard2);
		Task task5=new Task(5L, "task1OfTaskState1OfDashboardOfUser2", taskstate3);
		Task task6=new Task(6L, "task2OfTaskState1OfDashboardOfUser2", taskstate3);
		
		TaskState taskstate4=new TaskState(4L, "taskState2OfDashboardOfUser2", dashboard2);
		Task task7=new Task(7L, "task3OfTaskState2OfDashboardOfUser2", taskstate4);
		Task task8=new Task(8L,"task4OfTaskState2OfDashboardOfUser2", taskstate4);
		
		
		
		//List<User> users=new ArrayList<>();
		users.add(user1);
		users.add(user2);
		write.writeUsers();
		
		//List<Dashboard> dashboards=new ArrayList<>();
		dashboards.add(dashboard1);
		dashboards.add(dashboard2);
		write.writeDashboards();
		
		//List<TaskState> tStates=new ArrayList<>();
		tStates.add(taskstate1);
		tStates.add(taskstate2);
		tStates.add(taskstate3);
		tStates.add(taskstate4);
		write.writeTaskStates(tStates);
		
		//List<Task> tasks=new ArrayList();
		tasks.add(task1);
		tasks.add(task2);
		tasks.add(task3);
		tasks.add(task4);
		tasks.add(task5);
		tasks.add(task6);
		tasks.add(task7);
		tasks.add(task8);
		write.writeTasks();
		
		//User u=userService.findById(2L);
		//System.out.println(u.getUsername());
		//Dashboard dashboard=dashboardService.findById(2L);
		//System.out.println(dashboard.getUser().getUsername());
		//TaskState ts=taskStateService.findById(4L);
		//System.out.println(ts.getDashboard().getTitle());
	}
}
