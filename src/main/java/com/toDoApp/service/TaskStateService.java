package com.toDoApp.service;

import com.toDoApp.model.TaskState;

public interface TaskStateService {

	TaskState findById(Long id);
	void addTaskState(TaskState taskState);
	void updateTaskState(TaskState taskState);
	void deketeTaskState(Long id);
}
