package com.toDoApp.service;

import com.toDoApp.model.User;

public interface UserService {

	User  findById(Long id);
	
}
