package com.toDoApp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toDoApp.TestData;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.TaskService;
import com.toDoApp.support.WriteIntoFiles;

@Service
public class TaskServiceImplementation implements TaskService {

	@Autowired
	private WriteIntoFiles write;
	
	@Override
	public Task findById(Long id) {
		Task task=null;
		for(Task t: TestData.tasks){
			if(t.getId().equals(id)){
				task=t;
				break;
			}
		}
		return task;
	}

	@Override
	public List<Task> getTasksOfTaskState(Long taskStateId) {
		List<Task> tasks=new ArrayList<>();
		for(Task t:TestData.tasks){
			if(t.getTaskState().getId().equals(taskStateId)){
				tasks.add(t);
			}
		}
		return tasks;
	}

	@Override
	public void addTask(Task task) {
		TestData.tasks.add(task);
		write.writeTasks();
	}

	@Override
	public void updateTask(Task task) {
		for(Task t:TestData.tasks){
			if(t.getId().equals(task.getId())){
				t=task;
				break;
			}
		}
		write.writeTasks();
	}

	@Override
	public void deleteTask(Long id) {
		for(Task t:TestData.tasks){
			if(t.getId().equals(id)){
				TestData.tasks.remove(t);
				break;
			}
		}
		write.writeTasks();
	}
	

}
