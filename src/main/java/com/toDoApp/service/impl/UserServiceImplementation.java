package com.toDoApp.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toDoApp.model.User;
import com.toDoApp.service.UserService;
import com.toDoApp.support.ReadDataFromFiles;

@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	private ReadDataFromFiles readData;
	
	@Override
	public User findById(Long id) {
		User user=null;
		ArrayList<User> users=readData.readUsers();
		for(User u:users){
			if(u.getId().equals(id)){
				user=u;
				break;
			}
		}
		return user;
	}

}
