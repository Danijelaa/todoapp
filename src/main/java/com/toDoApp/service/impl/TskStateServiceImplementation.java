package com.toDoApp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toDoApp.TestData;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.support.ReadDataFromFiles;
import com.toDoApp.support.WriteIntoFiles;

@Service
public class TskStateServiceImplementation implements TaskStateService {

	@Autowired
	private WriteIntoFiles write;
	@Autowired
	private ReadDataFromFiles readData;
	
	@Override
	public TaskState findById(Long id) {
		TaskState taskState=null;
	/*	for(TaskState ts:TestData.tStates){
			if(ts.getId().equals(id)){
				taskState=ts;
				break;
			}
		}*/
		ArrayList<TaskState> taskStates=readData.readTaskStates();
		for(TaskState ts: taskStates){
			if(ts.getId().equals(id)){
				taskState=ts;
				break;
			}
		}
		return taskState;
	}

	@Override
	public void addTaskState(TaskState taskState) {
		ArrayList<TaskState> taskStates=readData.readTaskStates();
		taskStates.add(taskState);
		write.writeTaskStates(taskStates);
	}

	@Override
	public void updateTaskState(TaskState taskState) {
		/*for(TaskState ts:TestData.tStates){
			if(ts.getId().equals(taskState.getId())){
				ts=taskState;
				break;
			}
		}
		write.writeTaskStates(TestData.tStates);*/
	}

	@Override
	public void deketeTaskState(Long id) {
	
	/*	List<Integer> forDelete=new ArrayList<Integer>();
		for(int i=0; i<TestData.tasks.size(); i++){
			if(TestData.tasks.get(i).getTaskState().getId().equals(id)){
				forDelete.add(i);
			}
		}
		
		
		
		for(TaskState ts:TestData.tStates){
			if(ts.getId().equals(id)){
				TestData.tStates.remove(ts);
				break;
			}
		}
		write.writeTaskStates();
		write.writeTasks();*/
	}

	
	
}
