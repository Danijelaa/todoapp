package com.toDoApp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.toDoApp.TestData;
import com.toDoApp.model.Dashboard;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.DashboardService;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.support.ReadDataFromFiles;
import com.toDoApp.web.dto.DashboardDTOAllData;
import com.toDoApp.web.dto.TaskDTOAllData;
import com.toDoApp.web.dto.TaskStateDTOAlldata;

@Service
public class DashboardServiceImplementation implements DashboardService {

	@Autowired
	TaskStateService taskStateService;
	@Autowired
	ReadDataFromFiles readData;
	
	@Override
	public Dashboard findById(Long id) {
		Dashboard dashboard=null;
	/*	for(Dashboard d:TestData.dashboards){
			if(d.getId().equals(id)){
				dashboard=d;
				break;
			}
		}*/
		ArrayList<Dashboard> dashboards=readData.readDashboards();
		for(Dashboard d: dashboards){
			if(d.getId().equals(id)){
				dashboard=d;
				break;
			}
		}
		return dashboard;
	}

	@Override
	public DashboardDTOAllData getAllData(Long id) {
		Dashboard d=findById(id);
		DashboardDTOAllData dashboard=new DashboardDTOAllData();
		dashboard.setId(d.getId());
		dashboard.setTitle(d.getTitle());
		
		List<TaskStateDTOAlldata> tss=new ArrayList<>();
		for(TaskState ts:TestData.tStates){
			if(ts.getDashboard().getId().equals(id)){
				TaskStateDTOAlldata taskstate=new TaskStateDTOAlldata();
				taskstate.setId(ts.getId());
				taskstate.setTitle(ts.getTitle());
				tss.add(taskstate);
				ArrayList<TaskDTOAllData> tasks=new ArrayList<>();
				for(Task t:TestData.tasks){
					if(t.getTaskState().equals(ts)){
						TaskDTOAllData task=new TaskDTOAllData();
						task.setId(t.getId());
						task.setTitle(t.getTitle());
						tasks.add(task);
					}
				}
				taskstate.setTasks(tasks);
			}
			
		}
		dashboard.setTaskstates(tss);
		
		return dashboard;
	}

}
