package com.toDoApp.service;

import java.util.List;

import com.toDoApp.model.Task;

public interface TaskService {

	Task findById(Long id);
	List<Task> getTasksOfTaskState(Long taskStateId);
	void addTask(Task task);
	void updateTask(Task task);
	void deleteTask(Long id);
}
