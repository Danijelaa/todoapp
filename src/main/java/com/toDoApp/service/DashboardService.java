package com.toDoApp.service;

import com.toDoApp.model.Dashboard;
import com.toDoApp.web.dto.DashboardDTOAllData;

public interface DashboardService {
	Dashboard findById(Long id);
	DashboardDTOAllData getAllData(Long id);
}
