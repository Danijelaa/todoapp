package com.toDoApp.web.dto;

public class TaskDTO {

	private Long id;
	private String title;
	private Long taskStateId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getTaskStateId() {
		return taskStateId;
	}
	public void setTaskStateId(Long taskStateId) {
		this.taskStateId = taskStateId;
	}
	
	
}
