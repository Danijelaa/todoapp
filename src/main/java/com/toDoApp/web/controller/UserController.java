package com.toDoApp.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.toDoApp.service.DashboardService;
import com.toDoApp.web.dto.DashboardDTOAllData;

@RestController
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	DashboardService dashboardService;
	
	@RequestMapping(method=RequestMethod.GET, value="/dashboards/{id}")
	ResponseEntity<DashboardDTOAllData> getDashboard(@PathVariable Long id){
		DashboardDTOAllData dash=dashboardService.getAllData(id);
		return new ResponseEntity<DashboardDTOAllData>(dash, HttpStatus.OK);
	}
}
