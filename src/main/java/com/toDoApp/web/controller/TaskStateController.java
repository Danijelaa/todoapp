package com.toDoApp.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.TaskService;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.support.TaskStateDTOToTaskState;
import com.toDoApp.support.TaskStateToTaskStateDTO;
import com.toDoApp.support.TaskToTaskDTO;
import com.toDoApp.support.Validation;
import com.toDoApp.web.dto.TaskDTO;
import com.toDoApp.web.dto.TaskStateDTO;

@RestController
@RequestMapping(value="/task-states")
public class TaskStateController {

	@Autowired
	TaskService taskService;
	@Autowired
	TaskToTaskDTO toTaskDto;
	@Autowired
	TaskStateService taskStateService;
	@Autowired
	TaskStateToTaskStateDTO toTaskStateDto;
	@Autowired
	Validation validation;
	@Autowired
	TaskStateDTOToTaskState toTaskState;
	
	@RequestMapping(value="/{taskStateId}/tasks", method=RequestMethod.GET)
	ResponseEntity<List<TaskDTO>> readTasksOfTaskStates(@PathVariable Long taskStateId, @RequestParam Long userId){
		//provjera za sesiju
		//provjera da l' postoji task-state i da l' je od user-a
		TaskState taskState=taskStateService.findById(taskStateId);
		if(taskState==null || !taskState.getDashboard().getUser().getId().equals(userId)){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		List<Task> tasks=taskService.getTasksOfTaskState(taskStateId);
		if(tasks.size()==0){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toTaskDto.convert(tasks), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{taskStateId}", method=RequestMethod.GET)
	ResponseEntity<TaskStateDTO> readTaskState(@PathVariable Long taskStateId,  @RequestParam Long userId){
		//provjera za sesiju
		//provjera da l' postoji taskState i da l' je od user-a
		TaskState taskState=taskStateService.findById(taskStateId);
		if(taskState==null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if(!taskState.getDashboard().getUser().getId().equals(userId)){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<TaskStateDTO>(toTaskStateDto.convert(taskState), HttpStatus.OK);
		
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<TaskStateDTO> createTaskState(@RequestBody TaskStateDTO newTaskStateDto,  @RequestParam Long userId){
		//provjera sesije
		boolean validated=validation.validateNewTaskState(newTaskStateDto, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		TaskState taskState=toTaskState.convert(newTaskStateDto);
		taskStateService.addTaskState(taskState);
		return new ResponseEntity<TaskStateDTO>(toTaskStateDto.convert(taskState), HttpStatus.CREATED);
		
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{taskStateId}")
	ResponseEntity<TaskStateDTO> updateTaskState(@RequestBody TaskStateDTO updatedTaskStateDto, @PathVariable Long taskStateId, @RequestParam Long userId){
		//provjera sesije
		if(updatedTaskStateDto.getId()==null || !updatedTaskStateDto.getId().equals(taskStateId)){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		boolean validated=validation.validateUpdatedTaskState(updatedTaskStateDto, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		TaskState taskState=toTaskState.convert(updatedTaskStateDto);
		taskStateService.updateTaskState(taskState);
		return new ResponseEntity<TaskStateDTO>(toTaskStateDto.convert(taskState), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{taskStateId}")
	ResponseEntity<?> deleteTaskState(@PathVariable Long taskStateId, @RequestParam Long userId){
		boolean validated=validation.validateTaskStateForDeleting(taskStateId, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		taskStateService.deketeTaskState(taskStateId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
	
}
