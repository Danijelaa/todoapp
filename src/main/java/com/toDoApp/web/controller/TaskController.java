package com.toDoApp.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.toDoApp.model.Task;
import com.toDoApp.service.TaskService;
import com.toDoApp.support.TaskDTOToTask;
import com.toDoApp.support.TaskToTaskDTO;
import com.toDoApp.support.Validation;
import com.toDoApp.web.dto.TaskDTO;

@RestController
@RequestMapping(value="/tasks")
public class TaskController {

	@Autowired
	TaskService taskService;
	@Autowired
	TaskToTaskDTO toTaskDto;
	@Autowired
	Validation validation;
	@Autowired
	TaskDTOToTask toTask;
	
	@RequestMapping(method=RequestMethod.GET, value="/{taskId}")
	ResponseEntity<TaskDTO> readTask(@PathVariable Long taskId, @RequestParam Long userId){
		//provjera sesije
		//provjera da l' je od user-a
		
		Task task=taskService.findById(taskId);
		if(task==null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if(!task.getTaskState().getDashboard().getUser().getId().equals(userId)){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<TaskDTO>(toTaskDto.convert(task), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<TaskDTO> createTask(@RequestBody TaskDTO newTaskDto, @RequestParam Long userId){
		//provjera sesije
		boolean validated=validation.validateNewTask(newTaskDto, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Task newTask=toTask.convert(newTaskDto);
		taskService.addTask(newTask);
		return new ResponseEntity<TaskDTO>(toTaskDto.convert(newTask), HttpStatus.CREATED);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{taskId}")
	ResponseEntity<TaskDTO> updateTask(@RequestBody TaskDTO updatedTaskDto, @PathVariable Long taskId, @RequestParam Long userId){
		//provjera za sesiju
		if(updatedTaskDto.getId()==null || !updatedTaskDto.getId().equals(taskId)){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		boolean validated=validation.validateUpdatedTask(updatedTaskDto, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 
		Task task=toTask.convert(updatedTaskDto);
		taskService.updateTask(task);
		return new ResponseEntity<TaskDTO>(toTaskDto.convert(task), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{taskId}")
	ResponseEntity<?> deleteTask(@PathVariable Long taskId, @RequestParam Long userId){
		//provjera sesije
		//provjera da l' postoji task i da l' je od usera
		boolean validated=validation.validateTaskForDeleting(taskId, userId);
		if(!validated){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		taskService.deleteTask(taskId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
}
