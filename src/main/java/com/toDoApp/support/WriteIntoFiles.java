package com.toDoApp.support;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.toDoApp.TestData;
import com.toDoApp.model.Dashboard;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.model.User;
@Component
public class WriteIntoFiles {

	public void writeTasks(){
		
		File path=new File("./src/main/resources/tasks.csv");
		BufferedWriter br=null;
		try {
			br=new BufferedWriter(new FileWriter(path));
			String line;
			for(Task t:TestData.tasks){
				line=t.getId().toString()+","+t.getTitle()+","+t.getTaskState().getId().toString();
				br.write(line+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(br!=null){
				try {
					br.flush();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public void writeTaskStates(List<TaskState> taskStates){
		File path=new File("./src/main/resources/task-states.csv");
		BufferedWriter br=null;
		try {
			br=new BufferedWriter(new FileWriter(path));
			String line;
			for(TaskState ts:taskStates){
				line=ts.getId().toString()+","+ts.getTitle()+","+ts.getDashboard().getId().toString();
				br.write(line+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(br!=null){
				try {
					br.flush();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void writeDashboards(){
		File path=new File("./src/main/resources/dashboards.csv");
		
		BufferedWriter br=null;
		try {
			br=new BufferedWriter(new FileWriter(path));
			String line;
			for(Dashboard d:TestData.dashboards){
				line=d.getId().toString()+","+d.getTitle()+","+d.getUser().getId().toString();
				br.write(line+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(br!=null){
				try {
					br.flush();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void writeUsers(){
		File path=new File("./src/main/resources/users.csv");
		BufferedWriter br=null;
		try {
			br=new BufferedWriter(new FileWriter(path));
			String line;
			for(User u:TestData.users){
				line=u.getId().toString()+","+u.getUsername()+","+u.getPassword();
				br.write(line+"\n");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(br!=null){
				try {
					br.flush();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
