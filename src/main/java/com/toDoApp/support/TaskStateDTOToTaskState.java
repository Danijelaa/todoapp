package com.toDoApp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.toDoApp.model.TaskState;
import com.toDoApp.service.DashboardService;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.web.dto.TaskStateDTO;

@Component
public class TaskStateDTOToTaskState implements Converter<TaskStateDTO, TaskState> {

	@Autowired
	TaskStateService taskStateService;
	@Autowired
	DashboardService dashboardService;
	
	@Override
	public TaskState convert(TaskStateDTO taskStateDto) {
		TaskState taskState;
		if(taskStateDto.getId()==null){
			taskState=new TaskState();
			Long id=TaskState.getIndex();
			taskState.setId(id);
			TaskState.setIndex(id+1L);
		}
		else{
			taskState=taskStateService.findById(taskStateDto.getId());
		}
		taskState.setDashboard(dashboardService.findById(taskStateDto.getDashboardId()));
		taskState.setTitle(taskStateDto.getTitle());
		return taskState;
	}

}
