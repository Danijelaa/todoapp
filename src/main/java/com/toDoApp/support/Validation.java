package com.toDoApp.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import com.toDoApp.model.Dashboard;
import com.toDoApp.model.Task;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.DashboardService;
import com.toDoApp.service.TaskService;
import com.toDoApp.service.TaskStateService;
import com.toDoApp.web.dto.TaskDTO;
import com.toDoApp.web.dto.TaskStateDTO;

@Component
public class Validation {

	@Autowired
	private TaskStateService taskStateService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private DashboardService dashboardService;
	
	public boolean validateNewTask(TaskDTO newTaskDto, Long userId){
		if(newTaskDto.getId()!=null || newTaskDto.getTitle()==null || newTaskDto.getTaskStateId()==null){
			return false;
		}
		TaskState taskState=taskStateService.findById(newTaskDto.getTaskStateId());
		if(taskState==null || !taskState.getDashboard().getUser().getId().equals(userId)){
			return false;
		}
		return true;
	}
	
	public boolean validateUpdatedTask(TaskDTO updatedTaskDto, Long userId){
		Task task=taskService.findById(updatedTaskDto.getId());
		//provjera da l' postoji task i da l' su unijeti svi parametri(id je provjeren u kontroleru)
		if(task==null || updatedTaskDto.getTitle()==null || updatedTaskDto.getTaskStateId()==null){
			return false;
		}
		//provjera da l' postoji taskState i da li je  od usera
		TaskState taskState=taskStateService.findById(updatedTaskDto.getTaskStateId());
		if(taskState==null || !taskState.getDashboard().getUser().getId().equals(userId)){
			return false;
		}
		//provjera da l' je na istom dashboardu izvrsena promjena taska????
		if(!task.getTaskState().getDashboard().equals(taskState.getDashboard())){
			return false; 
		}
		return true;
	}
	
	public boolean validateTaskForDeleting(Long taskId, Long userId){
		Task task=taskService.findById(taskId);
		if(task==null || !task.getTaskState().getDashboard().getUser().getId().equals(userId)){
			return false;
		}
		return true;
	}
	
	public boolean validateNewTaskState(TaskStateDTO newTaskStateDto, Long userId){
		if(newTaskStateDto.getId()!=null || newTaskStateDto.getDashboardId()==null || newTaskStateDto.getTitle()==null){
			return false;
		}
		Dashboard dashboard=dashboardService.findById(newTaskStateDto.getDashboardId());
		if(dashboard==null || !dashboard.getUser().getId().equals(userId)){
			return false;
		}
		return true;
	}
	
	
	public boolean validateUpdatedTaskState(TaskStateDTO updatedTaskStateDto, Long userId){
		TaskState taskState=taskStateService.findById(updatedTaskStateDto.getId());
		if(taskState==null || updatedTaskStateDto.getDashboardId()==null || updatedTaskStateDto.getTitle()==null){
			return false;
		}
		if(!taskState.getDashboard().getUser().getId().equals(userId)){
			return false;
		}
		if(!taskState.getDashboard().getId().equals(updatedTaskStateDto.getDashboardId())){
			return false;
		}
		return true;
	}
	
	public boolean validateTaskStateForDeleting(Long taskStateId, Long userId){
		TaskState taskState=taskStateService.findById(taskStateId);
		if(taskState==null || !taskState.getDashboard().getUser().getId().equals(userId)){
			return false;
		}
		return true;
	}
	
}
