package com.toDoApp.support;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toDoApp.TestData;
import com.toDoApp.model.Dashboard;
import com.toDoApp.model.TaskState;
import com.toDoApp.model.User;
import com.toDoApp.service.DashboardService;
import com.toDoApp.service.UserService;
@Component
public class ReadDataFromFiles {

	@Autowired
	private UserService userService;
	@Autowired
	private DashboardService dashboardService;
	
	public ArrayList<User> readUsers(){
		ArrayList<User> users=new ArrayList<>();
		File path=new File("./src/main/resources/users.csv");
		BufferedReader reader=null;
		try {
			reader=new BufferedReader(new FileReader(path));
			String line;
			try {
				while((line=reader.readLine())!=null){
					String[] array=line.split(",");
					User user=new User(Long.parseLong(array[0]), array[1], array[2]);
					users.add(user);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(reader!=null){
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return users;
	}
	public ArrayList<Dashboard> readDashboards(){
		ArrayList<Dashboard> dashboards=new ArrayList<>();
		File path=new File("./src/main/resources/dashboards.csv");
		BufferedReader reader=null;
		try {
			reader=new BufferedReader(new FileReader(path));
			String line;
			try {
				while((line=reader.readLine())!=null){
					String[] array=line.split(",");
					User user=userService.findById(Long.parseLong(array[2]));
					Dashboard dashboard=new Dashboard(Long.parseLong(array[0]), array[1], user );
					dashboards.add(dashboard);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(reader!=null){
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return dashboards;
	}
	public ArrayList<TaskState> readTaskStates(){
		 ArrayList<TaskState> taskStates=new ArrayList<>();
		 File path=new File("./src/main/resources/task-states.csv");
			BufferedReader reader=null;
			try {
				reader=new BufferedReader(new FileReader(path));
				String line;
				try {
					while((line=reader.readLine())!=null){
						String[] array=line.split(",");
						Dashboard dashboard=dashboardService.findById(Long.parseLong(array[2]));
						TaskState taskstate=new TaskState(Long.parseLong(array[0]), array[1], dashboard);
						taskStates.add(taskstate);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				if(reader!=null){
					try {
						reader.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		 return taskStates;
	}
	
	
}
