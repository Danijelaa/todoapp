package com.toDoApp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.toDoApp.model.Dashboard;
import com.toDoApp.model.TaskState;
import com.toDoApp.service.DashboardService;
import com.toDoApp.web.dto.TaskStateDTO;

@Component
public class TaskStateToTaskStateDTO implements Converter<TaskState, TaskStateDTO>{

	/*@Autowired
	private DashboardService dashboardService;*/
	
	@Override
	public TaskStateDTO convert(TaskState taskState) {
		TaskStateDTO taskStateDto=new TaskStateDTO();
		taskStateDto.setId(taskState.getId());
		taskStateDto.setTitle(taskState.getTitle());
		taskStateDto.setDashboardId(taskState.getDashboard().getId());
		return taskStateDto;
	}

	public List<TaskStateDTO> convert(List<TaskState> taskStates){
		List<TaskStateDTO> taskStatesDtos=new ArrayList<>();
		for(TaskState ts:taskStates){
			taskStatesDtos.add(convert(ts));
		}
		return taskStatesDtos;
	}
}
